import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground ,Image, TouchableOpacity} from 'react-native';



export default function App() {
  
  return (
    
  <View style={styles.container}>
    <ImageBackground style={styles.bg} source={require('./../Image/bg.jpeg')}>
    <StatusBar style="auto" />
    <View style={styles.layergelap}/>
      <View style= {styles.mid}>
        <Image source={require('./../Image/lg.png')} style= {styles.logo}/>
      </View>
      <View style= {styles.mid}>
        <Text style={styles.title}>Deep talk and</Text>
        <Text style={styles.title}>secret</Text>
      </View>
      <View style= {styles.mid}>
        <Text style={styles.desc}>Let out your anger without</Text>
        <Text style={styles.desc}>hesitation!</Text>
      </View>
      <View style= {styles.mid}>
        <Text style={styles.des}>Join SyakirApp today.</Text>
      </View>
      
      <View style= {styles.mid}>
        <TouchableOpacity
          style={styles.btnlogin}     
          onPress={() => window.location.href = 'Login.js'}>
          <View style= {styles.mid}>
            <Text style= {styles.login}>Log in</Text>
          </View>
        </TouchableOpacity>
      </View>
      
    <View style= {styles.mid}>
      <TouchableOpacity style={styles.btnsignup}>
        <View style= {styles.mid}>
          <Text style= {styles.signup}>Sign up</Text>
        </View>
      </TouchableOpacity>
    </View>
    </ImageBackground>
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  bg: {
    width:360, 
    height:640
  },

  logo: {
    top : -600,
    alignItems:'center',
    justifyContent: 'center',
    height: 136,
    width: 113
  },

  layergelap:{
    backgroundColor:'black',
    width: '100%',
    height:'100%',
    opacity: 0.7,
  },

  title:{
    top: -595,
    fontSize: 36,
    color: 'white'
  },

  desc: {
    top: -580,
    fontSize: 14,
    color: '#868686'
  },

  des: {
    top: -470,
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold'
  },

  mid:{
    alignItems:'center'
  },

  btnlogin: {
    position:'absolute',
    borderRadius:8,
    alignItems:'center',
    top: -440,
    backgroundColor: "#DDDDDD23",
    width:198,
    height:36
  },

  login:{
    alignItems: 'center',
    fontSize: 21,
    color: 'white'
  },

  btnsignup: {
    position:'absolute',
    borderRadius:8,
    alignItems:'center',
    top: -390,
    backgroundColor: "#DDDDDD23",
    width:198,
    height:36
  },

  signup:{
    alignItems: 'center',
    fontSize: 21,
    color: 'white'
  }
});

