import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground ,Image,SafeAreaView,TextInput, TouchableOpacity} from 'react-native';



export default function App() {
  const [text,onChangeText]=React.useState()
  
  const [text1,onChangeText1]=React.useState()
  return (
      
<View style={styles.container}>
  <ImageBackground style={styles.bg} source={require('./../Image/bg1.png')}>
  <StatusBar style="auto" />
  <View style={styles.layergelap}/>
    <View style= {styles.mid}>
      <Image source={require('./../Image/back.png')} style= {styles.back}/>
    </View>
    <View style= {styles.mid}>
      <Image source={require('./../Image/lg.png')} style= {styles.logo}/>
    </View>
    <View style= {styles.mid}>
      <Text style={styles.title}>Log in to Syakir</Text>
    </View>
      
    <SafeAreaView>
      <View style= {styles.mid}>
        <TextInput
            style={styles.texusername}
            placeholder=' Phone, email, or username'
            placeholderTextColor= 'white'
            onChangeText={onChangeText} value={text}/>
      </View>
    </SafeAreaView>
    <View style= {styles.mid}>
      <View style= {styles.mid}>
        <SafeAreaView>
          <TextInput
            style={styles.texpass}
            placeholder=' Password'
            placeholderTextColor= 'white'
            onChangeText1={onChangeText1} value={text1}/>
        </SafeAreaView>
      </View>
      <TouchableOpacity
          style={styles.btnlogin}     
          onPress={() => window.location.href = 'Login.js'}>
        <View style= {styles.mid}>
          <Text style= {styles.login}>Log in</Text>
        </View><View style= {styles.mid}>
      <Text style={styles.des}>Forgot password? . Sign up for Syakir</Text>
    </View>
      </TouchableOpacity>
      
    </View>
  </ImageBackground>
</View>
);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  bg: {
    width:360, 
    height:640
  },

  input: {
    flex:1,
    position: 'absolute',
    color: 'white'
  },

back: {
    top : -580,
    right: 150,
    alignItems:'center',
    justifyContent: 'center',
    height: 21,
    width: 45
  },

  logo: {
    top : -530,
    right: 105,
    alignItems:'center',
    justifyContent: 'center',
    height: 76,
    width: 93
  },

  layergelap:{
    backgroundColor:'black',
    width: '100%',
    height:'100%',
    opacity: 0.2,
  },

  title:{
    top: -515,
    right: 52,
    fontSize: 24,
    color: 'white'
  },

  des: {
    bottom: -26,
    fontSize: 11,
    color: 'white',
    fontWeight: 'bold'
  },

  mid:{
    alignItems:'center'
  },

  btnlogin: {
    position:'absolute',
    borderRadius:100,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom: 320,
    backgroundColor: "#DDDDDD70",
    width:262,
    height:36
  },

  login:{
    alignItems: 'center',
    fontSize: 21,
    color: 'white'
  },

  texusername: {
    position:'absolute',
    borderRadius:8,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom : 450,
    backgroundColor: "#DDDDDD20",
    width:262,
    height:36
  },

  texpass: {
    position: 'relative',
    borderRadius:8,
    borderColor: 'white',
    borderWidth: 1,
    alignItems:'center',
    bottom : 410,
    backgroundColor: "#DDDDDD20",
    width:262,
    height:36
  },

  tex:{
    top: 9,
    left: -52,
    fontSize: 13,
    color: 'white'
  },

  tex1:{
    position: 'relative',
    top: 9,
    left: -99,
    fontSize: 13,
    color: 'white'
  }

});

