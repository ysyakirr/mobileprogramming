// Tambahkan while loop dibawah

let number=2;
console.log("LOOPING PERTAMA");
while (number<=20){
  console.log(number+" - I love coding");
  number+=2;
}

console.log(" ");

let number2 =20;
console.log("LOOPING KEDUA");
while (number2<=20 && number2 >=2 ){
    console.log(number2+" - I will become a mobile developer");
    number2-=2;
}
console.log(" ");
console.log(" ");


//looping menggunakan for
for (let number3= 1; number3<=20; number3+=1){
  if( number3 % 3== 0 && number3 %2 !=0){
    console.log(number3 +" - I Love Coding");
  }else if( number3 % 2!=0){
    console.log(number3 +" - Teknik");
  }else if ( number3 % 2== 0){
    console.log(number3+" - Informatika");
  }
}

console.log("");
console.log("");


//membuat persegi panjang
let n ="";
for (let i=1; i<=4; i++){
  for (let j=1; j<=8; j++){
    n += "#";
   
  }
  n += "\n";
}
console.log(n);
console.log("");


//membuat tangga
let m ="";
for (let i=1; i<=1; i++){
  for (let j=1; j<=7; j++){
    m += "#";
    console.log(m);
  }
  m += "#";
}


console.log("");


//membuat papan catur
let y ="";
for (let i=1; i<=8; i++){
  for (let j=1; j<=8; j++){
    if (i % 2 !=0 && j % 2 != 0 ){
      y += " #";
    }else if (i % 2 ==0 && j%2 ==0){
      y += "# ";
    }
    
    } y += "\n";
  
}
console.log(y);
