console.log("      range");
console.log('==================');
//range
var range = (angka1, angka2)=>{
    var array =[];
    if ( angka1<angka2){
        for (i=angka1; i<=angka2; i++){
        array.push(i)
    }
    }
    else if(angka1>angka2){
        for (j=angka2; j<=angka1; j++){
            array.push(j)
        }
    }
    else if(angka1==0 && angka2==0){
        array.push(-1)
    }return array;
    }

console.log(range(3,7));


console.log("");
console.log(' range with step');
console.log('==================');
//range with step
var rangews = (p1, p2, p3)=>{
    var ar =[];
    if ( p1<p2){
        for (i=p1; i<=p2; i+=p3){
        ar.push(i)
    }
    }
    else if(p1>p2){
        for (j=p1; j>=p2; j-=p3){
            ar.push(j)
        }
    }
    else if(p1==0 && p2==0){
        ar.push(-1)
    }return ar;
    }

console.log(rangews(1,10, 2));
console.log(rangews(11,23, 3));
console.log(rangews(5,2, 1));
console.log(rangews(29,2, 4));


console.log('');
console.log('   sum of range');
console.log('==================');
//sum of range
var jumlah = (a, b, c) => {
    var array = [];
    if (a== null && b== null && c == null) {
        array.push(0);
        var sum = array[0];
    }
    else if (a < b && c == null) {
        for (var i = a; i <= b; i++) {
            array.push(i);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    } 
    else if (a > b && c == null) {
        for (var j = a; j >= b; j--) {
            array.push(j);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    } 
    else if (a < b) {
        for (var k = a; k <= b; k += c) {
            array.push(k);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    } 
    else if (a > b) {
        for (var l = a; l >= b; l -= c) {
            array.push(l);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    } 
    else if (b == null && c == null) {
        array.push(1);
        var sum = array.reduce((a, b) => { return a+b ; }, 0)
    } 
    
    
    return sum;
}
    console.log(jumlah(1,10)) // 55
    console.log(jumlah(5, 50, 2)) // 621
    console.log(jumlah(15, 10)) // 75
    console.log(jumlah(20, 10, 2)) // 90
    console.log(jumlah(1)) // 1
    console.log(jumlah()) // 0

console.log('');
console.log('array multidimensi');
console.log('==================');
//array multidimensi
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
    ["0008", "Yusuf Syakir", "Purwakarta", "08/05/2000", "Watching Movie"]
            ]
            
var proses1 = 0;
var proses2 = input.length;
var output = "";

function dataHandling (input){
for(proses1; proses1 < proses2; proses1++){
    output += 'Nomor ID: ' + input [proses1] [0] + '\n' +
      'Nama Lengkap: ' + input [proses1] [1] + '\n' +
      'TTL: ' + input [proses1] [2]+ ', '+input [proses1] [3] + '\n' +
      'Hobi: ' + input [proses1] [4] + '\n' + '\n';

}return output;
}

console.log(dataHandling (input));



console.log('');
console.log('   balik kata');
console.log('==================');

function balikKata(str){
    var balik = str;
    var kata = '';

    for (let i = str.length- 1; i>=0; i--){
        kata = kata + balik[i];
    }return kata;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("Informatika")) // akitamrofnI
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Humanikers")) // srekinamuH ma I

console.log('');
console.log('metode array');
//metode array
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(bio) {
    
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria" , "SMA Internasional Metro");
    console.log(input);
    
    var inputSplit = input[3].split("/");
    var inputJoin= inputSplit.join("-");
    var bulan = inputJoin[3] + inputJoin[4];
    
    switch(bulan) {
      case '01': console.log('Januari'); 
        break;
      case '02': console.log('Februari'); 
        break; 
      case '03': console.log('Maret'); 
        break; 
      case '04': console.log('April'); 
        break; 
      case '05': console.log ('Mei'); 
        break; 
      case '06': console.log('Juni'); 
        break; 
      case '07': console.log('Juli'); 
        break; 
      case '08': console.log('Agustus'); 
        break; 
      case '09': console.log('September'); 
        break; 
      case '10': console.log('Oktober'); 
        break; 
      case '11': console.log('November'); 
        break; 
      case '12': console.log('Desember'); 
        break; 
    }
    
    var inputShortDes = inputSplit.sort(function(a, b) {return a - b});
    console.log(inputShortDes);
    
    var tes = input[3].split("/");
    console.log(tes.join("-"));
    
    console.log(input[1].toString().slice(0,14));
    return bio;
    }
    dataHandling2();
