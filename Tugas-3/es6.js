console.log('No.1 - Mengubah fungsi menjadi fungsi arrow')
console.log('')
const golden = ()=>{
    console.log("this is golden!!")
    }
    golden()

console.log('')
console.log('')




console.log('No.2 - Sederhanakan menjadi Object literal di ES6')
console.log('')
const newFunction = (firstName, lastName)=>{
    return {
        firstName: firstName,
        lastName: lastName,
        fullName:() =>
        console.log(firstName + " " + lastName)
        }
        }
        newFunction("William", "Imoh").fullName()

console.log('')
console.log('')


console.log('No.3 - Destructuring')
console.log('')
    const newObject = ["Harry","Potter Holt","Hogwarts React Conf","Deve-wizard Avocado","Vimulus Renderus!!!"
]
const [firstName, lastName, destination, occupation] = newObject;
console.log(firstName, lastName, destination, occupation)

console.log('')
console.log('')


console.log('No.4 - Array Spreading')
console.log('')
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);
console.log('')
console.log('')


console.log('')
console.log('No.5 - Template Literals')

const planet = "earth"
const view = "glass"
var before = `Lorem  ${view}  dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before)

