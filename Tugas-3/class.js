console.log('No. 1 - Animal Class')
console.log('')


class Animal {
    // Code class di sini
    constructor(name) {
        this.AniName=name;
        this.AniLegs= 4;
        this.AniCold_blooded=false;
      }
      get name(){
          return this.AniName;
      }
      get legs(){
          return this.AniLegs;
      }
      get cold_blooded(){
          return this.AniCold_blooded;
      }

      set name(x){
          this.AniName=x;
      }
      set legs(y){
          this.AniLegs=y;
      }
      set cold_blooded(z){
          this.AniCold_blooded=z;
      }


    }


    // Code class Ape dan class Frog di sini
    class Ape extends Animal{
        constructor(name){
        super(name)
        this.AniName=name;
        this.AniLegs=2;
       }yell(){
           return console.log('auoo')
       }
            }

    class Frog extends Animal{
        constructor(name){
        super(name)
        this.AniName=name;
        }jump(){
            return console.log('hop hop')
        }
            }

        let sheep = new Animal("shaun");
        console.log(sheep.name) // "shaun"
        console.log(sheep.legs) // 4
        console.log(sheep.cold_blooded) // false
        console.log('')
    
    
        let sungokong = new Ape("kera sakti")
        console.log(sungokong.name) // "shaun"
        console.log(sungokong.legs) // 4
        console.log(sungokong.cold_blooded) // false
        sungokong.yell() // "Auooo"
        console.log('')
        
        let kodok = new Frog("buduk")
        console.log(kodok.name) // "shaun"
        console.log(kodok.legs) // 4
        console.log(kodok.cold_blooded) // false
        kodok.jump() // "hop hop"
        console.log('')







console.log('')
console.log('')


console.log('')
console.log('No.2 - Function to Class')
class Clock{
    constructor({ template }) {
        this.template = template
    }
    render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

    console.log(output);
    }
    stop() {
    clearInterval(timer);
    };
    start() {
        this.render();
        this.timer = setInterval(()=> this.render(),1000);
  
    }
    
}
    let clock = new Clock({template: 'h:m:s'});
    clock.start();